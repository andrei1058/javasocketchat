package com.andrei1058.jschat.database;

import com.andrei1058.jschat.errors.NoDatabaseConnection;
import org.sqlite.JDBC;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Database {

    private static Database instance;

    private Connection connection;
    private String databaseName;
    private String settings = "settings";

    /**
     * Singleton database instance.
     */
    private Database(String username) throws SQLException, IOException {
        this.databaseName = username;
        instance = this;

        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            DriverManager.registerDriver(new JDBC());
        }
        File folder = new File(System.getProperty("user.home") + "/JavaSocketChat");
        if (!folder.exists()) folder.mkdir();

        File database = new File(folder.getPath() + "/" + databaseName + ".db");
        if (!database.exists()) {
            database.createNewFile();
        }

        connection = DriverManager.getConnection("jdbc:sqlite:" + database.getPath());
    }

    /**
     * Initialize database.
     */
    public static void init(String username) throws NoDatabaseConnection {
        if (instance != null) {
            instance.close();
        }
        try {
            new Database(username);
        } catch (Exception e) {
            throw new NoDatabaseConnection();
        }
        instance.createTables();
    }

    /**
     * Create required tables.
     */
    private void createTables() {
        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS " + settings + " (name VARCHAR(255), value INTEGER);");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get friends list version.
     * If the version on the remote is different the list will be updated.
     */
    public int getFriendsVersion() {
        int i = 0;
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT value FROM " + settings + " WHERE name = 'friends';");
            if (rs.next()) i = rs.getInt("value");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;
    }

    /**
     * Close database connection.
     */
    public void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static Database getInstance() {
        return instance;
    }
}
