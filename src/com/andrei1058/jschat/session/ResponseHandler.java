package com.andrei1058.jschat.session;

import java.util.LinkedList;

public abstract class ResponseHandler {

    private static LinkedList<ResponseHandler> responseHandlerList = new LinkedList<>();

    private Type type;

    /**
     * ResponseHandler type.
     */
    public ResponseHandler(Type requestType) {
        this.type = requestType;
        responseHandlerList.add(this);
    }

    /**
     * Get request type.
     */
    public Type getType() {
        return type;
    }

    /**
     * Handle request.
     */
    public abstract void execute(String[] args);

    /**
     * Get request executors.
     */
    public static LinkedList<ResponseHandler> getResponseHandlerList() {
        return responseHandlerList;
    }

    /**
     * ResponseHandler types.
     */
    public enum Type {
        /**
         * When there is a user with this name online.
         */
        ALREADY_CONNECTED,
        /**
         * When the provided username is not registered.
         */
        NOT_REGISTERED,
        /**
         * When the given username has an invalid format.
         */
        INVALID_USERNAME,

        /**
         * When provided password did not match.
         */
        WRONG_PASSWORD,

        /**
         * When the received request has an invalid format.
         */
        INVALID_REQUEST_FORMAT,

        /**
         * When login request has accepted.
         */
        LOGIN_SUCCESS,

        /**
         * When tried to register existing username.
         */
        ALREADY_REGISTERED,

        /**
         * When registered successfully.
         */
        REGISTER_SUCCESS,

        /**
         * When new friend request received.
         */
        FRIEND_REQUEST,

        /**
         * New friend to list.
         * <p>
         * username, conversation id
         */
        UPDATE_FRIEND_STATUS,

        /**
         * Friends list on login.
         * <p>
         * username, online_status - next user
         */
        FRIENDS_LIST_UPDATE,

        /**
         * Send message to a user.
         * <p>
         * username, message base64
         */
        MESSAGE,

        /**
         * Force log-out
         */
        LOGOUT,

        /**
         * Someone removed you from the friends list so
         * remove him from the friends list too.
         */
        REMOVED_FROM_FRIEND,
    }
}
