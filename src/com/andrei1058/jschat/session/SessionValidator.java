package com.andrei1058.jschat.session;

import com.andrei1058.jschat.database.Database;
import com.andrei1058.jschat.errors.*;

import java.net.UnknownHostException;
import java.util.regex.Pattern;

public class SessionValidator {

    private String username;
    private String password;

    /**
     * Validate session for login button.
     */
    public SessionValidator(String username, String password) throws InvalidUsernameFormat, NoDatabaseConnection, UnknownHostException, InvalidPassword {
        this.username = username;
        this.password = password;
        if (!validateUsername(username)) throw new InvalidUsernameFormat(username);
        if (!validatePassword()) throw new InvalidPassword(username);
        //Database.init(username);

        SessionHandler.openConnection(username, password);
    }

    /**
     * Validate session for register button.
     */
    public SessionValidator(String username, String password1, String password2, String email) throws PasswordsMatchException, InvalidUsernameFormat, InvalidPassword, InvalidEmailException, NoDatabaseConnection, UnknownHostException {
        this.username = username;
        this.password = password1;
        if (!validateUsername(username)) throw new InvalidUsernameFormat(username);
        if (!validatePassword()) throw new InvalidPassword(username);
        if (!password1.equals(password2)) throw new PasswordsMatchException();
        if (!isValidEmailAddress(email)) throw new InvalidEmailException(email);
        //Database.init(username);
        SessionHandler.openConnection(username, password, email);
    }

    /**
     * Check username syntax.
     */
    public static boolean validateUsername(String username) {
        if (username.isEmpty()) return false;
        if (username.length() <= 5 || username.length() >= 25) return false;
        if (username.equalsIgnoreCase("username")) return false;
        return Pattern.matches("[A-Za-z0-9_]+", username);
    }

    /**
     * Check password.
     */
    private boolean validatePassword() {
        return password.length() > 7;
    }

    /**
     * Validate email address.
     */
    private boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
}
