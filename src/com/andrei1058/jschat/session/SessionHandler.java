package com.andrei1058.jschat.session;

import com.andrei1058.jschat.Main;
import com.andrei1058.jschat.database.Database;
import com.andrei1058.jschat.ui.login.LoginView;
import com.andrei1058.jschat.ui.mainview.MainView;
import com.andrei1058.jschat.ui.register.RegisterView;
import com.andrei1058.jschat.utils.Logger;
import javafx.scene.Node;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Scanner;

public class SessionHandler {

    private static SessionHandler sessionHandler;

    private Socket socket;
    private String remote = "localhost";
    private int port = 6655;

    private PrintWriter out;
    private Scanner in;
    private boolean active = true;

    private SessionHandler(String username, String password, String email) throws UnknownHostException {
        sessionHandler = this;
        try {
            socket = new Socket(remote, port);
        } catch (IOException e) {
            sessionHandler = null;
            throw new UnknownHostException(remote + ":" + port);
        }

        new Thread(() -> {
            try {
                out = new PrintWriter(socket.getOutputStream(), true);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            try {
                in = new Scanner(socket.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            if (email == null) {
                sendLoginRequest(username, password);
            } else {
                sendRegisterRequest(username, password, email);
            }

            while (active) {
                if (in.hasNext()) {
                    String[] args = in.nextLine().split("<<-->>");
                    if (args.length == 0) return;
                    Logger.debug("New response: " + Arrays.toString(args));
                    ResponseHandler.Type type = ResponseHandler.Type.valueOf(args[0].toUpperCase());
                    switch (type) {
                        case NOT_REGISTERED:
                            LoginView.showMessage("Username not registered!", Color.RED);
                            break;
                        case WRONG_PASSWORD:
                            LoginView.showMessage("Wrong password!", Color.RED);
                            break;
                        case ALREADY_CONNECTED:
                            LoginView.showMessage("User already logged in from another location!", Color.RED);
                            break;
                        case LOGIN_SUCCESS:
                            onLoginAccepted(username);
                            break;
                        case INVALID_USERNAME:
                            LoginView.showMessage("Invalid message!", Color.RED);
                        case ALREADY_REGISTERED:
                            RegisterView.showMessage("Username already registered!", Color.RED);
                            break;
                        case REGISTER_SUCCESS:
                            RegisterView.showMessage("Registered successfully!", Color.GREEN);
                            onRegisterAccepted(username);
                            break;
                        case FRIEND_REQUEST:
                            MainView.getMainView().newFriendRequest(args[1]);
                            break;
                        case UPDATE_FRIEND_STATUS:
                            Friend fr = Friend.getByName(args[1]);
                            if (fr == null) {
                                new Friend(args[1], System.currentTimeMillis(), Integer.valueOf(args[2]));
                                continue;
                            } else {
                                fr.setStatus(Integer.valueOf(args[2]));
                            }
                            //MainView.getMainView().addFriendToList(args[1], Integer.valueOf(args[2]), System.currentTimeMillis());
                            break;
                        case FRIENDS_LIST_UPDATE:
                            if (args.length == 2) {
                                for (String friend : args[1].split("<<00>>")) {
                                    String[] data = friend.split("<<==>>");
                                    if (data.length == 0) continue;
                                    //username, status, last-message
                                    Friend f = Friend.getByName(data[0]);
                                    if (f == null) {
                                        new Friend(data[0], Long.valueOf(data[1]), Integer.valueOf(data[2]));
                                        continue;
                                    }
                                    f.setLastMessage(Long.valueOf(data[1]));
                                    f.setStatus(Integer.valueOf(data[2]));
                                    //MainView.getMainView().getRightSide().addFriendToList(data[0], Integer.valueOf(data[2]), Long.valueOf(data[1]));
                                }
                            }
                            break;
                        case MESSAGE:
                            User.getUser().receiveMessage(args[1], args[2]);
                            break;
                        /*default:
                            for (ResponseHandler r : ResponseHandler.getResponseHandlerList()) {
                                if (r.getType() == type) {
                                    r.execute(args.length > 1 ? Arrays.copyOfRange(args, 1, args.length) : new String[]{});
                                    break;
                                }
                            }
                            break;*/
                        case LOGOUT:
                            User.getUser().logOut();
                            break;
                        case REMOVED_FROM_FRIEND:
                            Friend f = Friend.getByName(args[1]);
                            if (f != null) f.remove();
                            if (MainView.getMainView() != null) {
                                Node n = MainView.getMainView().getRightSide().getNodeByUsername().get("friend_" + args[1]);
                                if (n != null) {
                                    Main.refreshUI(() -> {
                                        MainView.getMainView().getRightSide().getFriendsList().getChildren().remove(n);
                                        MainView.getMainView().getLeftSide().closeTab(args[1]);
                                    });
                                }
                            }
                            break;
                    }
                }
            }
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            in.close();
            out.close();
        }).start();
    }

    /**
     * Send login request to the remote host.
     */
    public void sendLoginRequest(String username, String password) {
        out.println("LOGIN<<-->>" + username + "<<-->>" + password);
    }

    /**
     * Things to do when the login request was accepted.
     */
    private void onLoginAccepted(String username) {
        LoginView.showMessage("Logging in..", Color.GREEN);
        User.login(username);
        Main.refreshUI(MainView::new);
    }

    /**
     * Things to do when the login request was accepted.
     */
    private void onRegisterAccepted(String username) {
        User.login(username);
        Main.refreshUI(MainView::new);
    }

    /**
     * Send register request to the remote host.
     */
    public void sendRegisterRequest(String username, String password, String email) {
        out.println("REGISTER<<-->>" + username + "<<-->>" + password + "<<-->>" + email);
    }

    /**
     * Send a message to the remote host.
     */
    public void sendMessage(String message) {
        out.println(message);
    }

    /**
     * Create session handler instance via login view.
     */
    public static void openConnection(String username, String password) throws UnknownHostException {
        if (getSessionHandler() == null) {
            new SessionHandler(username, password, null);
        } else {
            getSessionHandler().sendLoginRequest(username, password);
        }
    }

    /**
     * Create session handler instance via register view.
     */
    public static void openConnection(String username, String password, String email) throws UnknownHostException {
        if (getSessionHandler() == null) {
            new SessionHandler(username, password, email);
        } else {
            getSessionHandler().sendRegisterRequest(username, password, email);
        }
    }

    /**
     * Close connection.
     */
    public static void closeConnection() {
        if (sessionHandler != null) {
            sessionHandler.out.println("LOGOUT");
            sessionHandler.active = false;
            sessionHandler.in.close();
            sessionHandler.out.close();
            try {
                sessionHandler.socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        sessionHandler = null;
        ResponseHandler.getResponseHandlerList().clear();
    }

    /**
     * Get session handler instance.
     */
    public static SessionHandler getSessionHandler() {
        return sessionHandler;
    }
}
