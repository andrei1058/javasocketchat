package com.andrei1058.jschat.session;

import com.andrei1058.jschat.Main;
import com.andrei1058.jschat.ui.mainview.MainView;
import com.andrei1058.jschat.utils.Logger;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;

public class Friend implements Comparable {

    private String name;
    private long lastMessage;
    private int status;

    private static ArrayList<Friend> friendsList = new ArrayList<>();

    /**
     * Create a friend.
     */
    public Friend(String name, long lastMessage, int status) {
        this.name = name;
        this.lastMessage = lastMessage;
        this.status = status;
        friendsList.add(this);
        updateListOrder();
    }

    /**
     * Send message to a friend.
     *
     * @return false if could not send because user offline.
     */
    public void sendMessage(TextField textField) {
        if (textField.getText().isEmpty()) return;
        if (MainView.getMainView() == null) return;
        SessionHandler.getSessionHandler().sendMessage(ResponseHandler.Type.MESSAGE.toString() + "<<-->>" + name + "<<-->>" + Base64.getEncoder().encodeToString(textField.getText().getBytes()));
        Tab t = MainView.getMainView().getLeftSide().getTabByIdentifier().getOrDefault("friend_" + name, null);
        if (t == null) return;

        Main.refreshUI(() -> {
            if (status == 0) {
                MainView.getMainView().getLeftSide().showMessage(t, "System", "Could not send message: " + name + " is offline!", true);
            } else {
                Logger.debug("Sending message to: " + name + ": " + textField.getText());
                MainView.getMainView().getLeftSide().showMessage(t, User.getUser().getUsername(), textField.getText(), false);
            }
            textField.clear();
            textField.requestFocus();
        });
        setLastMessage(System.currentTimeMillis());
    }

    /**
     * Get friend by name.
     */
    public static Friend getByName(String name) {

        for (Friend f : friendsList) {
            if (f.name.equals(name)) return f;
        }

        return null;
    }

    public void setLastMessage(long lastMessage) {
        this.lastMessage = lastMessage;

        if (MainView.getMainView() == null) return;
        Main.refreshUI(() -> {
            Node n = MainView.getMainView().getRightSide().getNodeByUsername().get("friend_" + name);
            MainView.getMainView().getRightSide().getFriendsList().getChildren().remove(n);
            MainView.getMainView().getRightSide().addFriendToList(name, status, lastMessage);
        });
        updateListOrder();
    }

    public void setStatus(int status) {
        this.status = status;

        if (MainView.getMainView() == null) return;
        Main.refreshUI(() -> {
            Node n = MainView.getMainView().getRightSide().getNodeByUsername().get("friend_" + name);
            MainView.getMainView().getRightSide().getFriendsList().getChildren().remove(n);
            MainView.getMainView().getRightSide().addFriendToList(name, status, lastMessage);
        });
        updateListOrder();
    }

    /**
     * Triggered when the user session is closed.
     */
    public static void sessionClosed() {
        friendsList.clear();
    }

    @Override
    public int compareTo(Object o) {
        Friend f = (Friend) o;
        return Long.compare(f.lastMessage, lastMessage);
    }

    private static synchronized void updateListOrder() {
        if (MainView.getMainView() == null) return;
        Collections.sort(friendsList);
        Main.refreshUI(() -> {
            for (Node n : MainView.getMainView().getRightSide().getNodeByUsername().values()) {
                MainView.getMainView().getRightSide().getFriendsList().getChildren().remove(n);
            }
            for (Friend f : new ArrayList<>(friendsList)) {
                MainView.getMainView().getRightSide().addFriendToList(f.name, f.status, f.lastMessage);
            }
        });
    }

    public void remove(){
        friendsList.remove(this);
    }
}
