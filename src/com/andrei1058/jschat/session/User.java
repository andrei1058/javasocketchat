package com.andrei1058.jschat.session;

import com.andrei1058.jschat.Main;
import com.andrei1058.jschat.ui.login.LoginView;
import com.andrei1058.jschat.ui.mainview.MainView;
import com.andrei1058.jschat.utils.Logger;
import javafx.scene.control.Tab;

import java.util.Base64;

public class User {

    private static User user;

    private String username;

    private User(String username) {
        user = this;
        this.username = username;
    }

    /**
     * Add a friend.
     */
    public boolean addFriend(String user) {
        if (SessionValidator.validateUsername(user)) {
            SessionHandler.getSessionHandler().sendMessage("ADD_FRIEND<<-->>" + user);
            return true;
        }
        return false;
    }

    /**
     * Manage friend request.
     */
    public void manageFriendRequest(String user, boolean accept) {
        if (SessionValidator.validateUsername(user)) {
            SessionHandler.getSessionHandler().sendMessage("FRIEND_REQUEST_RESPONSE<<-->>" + user + "<<-->>" + accept);
        }
    }

    /**
     * Manage friend request.
     */
    public void removeFriend(String user) {
        if (SessionValidator.validateUsername(user)) {
            SessionHandler.getSessionHandler().sendMessage("REMOVE_FRIEND<<-->>" + user);
        }
        Friend f = Friend.getByName(user);
        if (f != null) f.remove();
    }

    /**
     * Create a group.
     */
    public boolean createGroup(String name) {
        //todo
        return false;
    }


    /**
     * Login user.
     */
    public static void login(String username) {
        new User(username);
    }

    /**
     * Logout user.
     */
    public void logOut() {
        SessionHandler.getSessionHandler().sendMessage("LOGOUT");
        Main.refreshUI(LoginView::new);
        SessionHandler.closeConnection();
        Friend.sessionClosed();
    }

    public void receiveMessage(String username, String message) {
        Logger.debug("Message received: " + username);
        Tab t = MainView.getMainView().getLeftSide().getTabByIdentifier().getOrDefault("friend_" + username, null);
        if (t == null) {
            MainView.getMainView().getLeftSide().showChat(username);
        }
        Friend f = Friend.getByName(username);
        if (f == null) return;
        f.setLastMessage(System.currentTimeMillis());

        new Thread(() -> {
            try {
                Thread.sleep(600);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Tab t2 = MainView.getMainView().getLeftSide().getTabByIdentifier().getOrDefault("friend_" + username, null);
            MainView.getMainView().getLeftSide().showMessage(t2, username, new String(Base64.getDecoder().decode(message)), false);

        }).start();
    }

    public static User getUser() {
        return user;
    }

    /**
     * Open a conversation tab with a friend.
     */
    public void openConversation(String username) {
        MainView.getMainView().getLeftSide().showChat(username);
    }

    public String getUsername() {
        return username;
    }
}
