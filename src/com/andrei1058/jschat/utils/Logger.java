package com.andrei1058.jschat.utils;

import java.util.Date;

public class Logger {

    /**
     * Show debug message.
     */
    public static void debug(String message) {
        System.out.println("[" + new Date(System.currentTimeMillis()) + " DEBUG] " + message);
    }
}
