package com.andrei1058.jschat.errors;

public class InvalidEmailException extends Throwable {

    private String email;

    public InvalidEmailException(String email) {
        this.email = email;
    }

    @Override
    public String getMessage() {
        return "Invalid email address: " + email;
    }
}
