package com.andrei1058.jschat.errors;

public class InvalidUsernameFormat extends Throwable {

    private String username;

    public InvalidUsernameFormat(String username) {
        this.username = username;
    }

    @Override
    public String getMessage() {
        return "Invalid username format: " + username;
    }
}
