package com.andrei1058.jschat.errors;

public class PasswordsMatchException extends Throwable {

    @Override
    public String getMessage() {
        return "Passwords don't match!";
    }
}
