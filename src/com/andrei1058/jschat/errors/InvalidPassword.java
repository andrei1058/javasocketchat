package com.andrei1058.jschat.errors;

public class InvalidPassword extends Throwable {

    private String username;

    public InvalidPassword(String username) {
        this.username = username;
    }

    @Override
    public String getMessage() {
        return "Invalid password for: " + username;
    }
}
