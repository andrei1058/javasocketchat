package com.andrei1058.jschat.errors;

public class SessionAlreadyInstantiated extends Throwable {

    @Override
    public String getMessage() {
        return "A client-server connection was already instantiated.";
    }
}
