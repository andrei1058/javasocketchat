package com.andrei1058.jschat.errors;

public class NoDatabaseConnection extends Throwable {

    @Override
    public String getMessage() {
        return "Could not connect to the local database!";
    }
}
