package com.andrei1058.jschat.ui.login;

import com.andrei1058.jschat.Main;
import com.andrei1058.jschat.errors.InvalidPassword;
import com.andrei1058.jschat.errors.InvalidUsernameFormat;
import com.andrei1058.jschat.errors.NoDatabaseConnection;
import com.andrei1058.jschat.session.SessionValidator;
import com.andrei1058.jschat.ui.register.RegisterView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.net.UnknownHostException;

public class LoginView {

    public static LoginView instance;
    private Text bottomMessage = new Text("");

    /**
     * Show the login screen.
     */
    public LoginView() {

        // Remove track of register view if instantiated
        if (RegisterView.getInstance() != null) RegisterView.setInstance(null);

        instance = this;
        Stage window = Main.getWindow();
        window.setTitle("Java Socket Chat v" + Main.version + " - Login Screen");

        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(new ImageView(new Image(LoginView.class.getResource("/header.png").toExternalForm())));

        VBox vBox = new VBox();
        HBox hBox = new HBox();
        vBox.getChildren().add(hBox);
        vBox.setAlignment(Pos.CENTER);
        vBox.setMaxWidth(600);
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(0, 0, 50, 0));

        VBox nameBox = new VBox();
        TextField nameField = new TextField();
        nameField.setPromptText("Click to insert");
        Text name = new Text("Username");
        nameBox.getChildren().add(name);
        nameBox.getChildren().add(nameField);

        VBox passBox = new VBox();
        PasswordField passField = new PasswordField();
        passField.setPromptText("Click to insert");
        Text pass = new Text("Password");
        passBox.getChildren().add(pass);
        passBox.getChildren().add(passField);

        Button login = new Button("Login");
        login.setOnMouseClicked(e -> {
            bottomMessage.setText("");
            onListener(nameField.getText(), passField.getText());
        });
        nameField.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                bottomMessage.setText("");
                onListener(nameField.getText(), passField.getText());
            }
        });
        passField.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                bottomMessage.setText("");
                onListener(nameField.getText(), passField.getText());
            }
        });

        StackPane stackPane = new StackPane();
        Button register = new Button("Register");
        register.setOnMouseClicked(e -> {
            bottomMessage.setText("");
            Main.refreshUI(RegisterView::new);
        });

        stackPane.getChildren().add(register);
        stackPane.setAlignment(Pos.BOTTOM_RIGHT);

        hBox.setAlignment(Pos.BOTTOM_CENTER);
        hBox.getChildren().add(nameBox);
        hBox.getChildren().add(passBox);
        hBox.getChildren().add(login);
        hBox.getChildren().add(stackPane);
        HBox.setHgrow(stackPane, Priority.ALWAYS);

        vBox.getChildren().add(bottomMessage);
        bottomMessage.setTextAlignment(TextAlignment.CENTER);
        VBox.setMargin(bottomMessage, new Insets(-30, 0, 0, 0));

        VBox.setVgrow(bottomMessage, Priority.ALWAYS);
        Hyperlink recover = new Hyperlink("Lost Password");
        vBox.getChildren().add(recover);

        borderPane.setBottom(vBox);
        BorderPane.setAlignment(vBox, Pos.CENTER);
        window.setMinHeight(510);
        window.setMinWidth(850);

        Scene scene = window.getScene() == null ? new Scene(borderPane) :
                new Scene(borderPane, window.getScene().getWidth(), window.getScene().getHeight());

        window.setScene(scene);
        window.show();
    }

    /**
     * Show a message on bottom.
     */
    public static void showMessage(String message, Color color) {
        if (instance == null) return;
        if (instance.bottomMessage.getText().isEmpty()) {
            if (color == Color.RED) {
                new Thread(() -> {
                    try {
                        Thread.sleep(4000);
                        if (instance != null) {
                            instance.bottomMessage.setText("");
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }).start();
            }
        }
        instance.bottomMessage.setText(message);
        instance.bottomMessage.setFill(color);
    }


    private void onListener(String username, String password) {
        //new MainView();
        try {
            new SessionValidator(username, password);
        } catch (InvalidUsernameFormat invalidUsernameFormat) {
            showMessage("Invalid username!", Color.RED);
        } catch (NoDatabaseConnection noDatabaseConnection) {
            showMessage("Could not connect to local database!", Color.RED);
        } catch (UnknownHostException ex) {
            showMessage("Could not connect to remote service!", Color.RED);
        } catch (InvalidPassword invalidPassword) {
            showMessage("Invalid password!", Color.RED);
        }
    }

    public static LoginView getInstance() {
        return instance;
    }

    public static void setInstance(LoginView instance) {
        LoginView.instance = instance;
    }
}
