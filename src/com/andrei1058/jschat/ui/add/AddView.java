package com.andrei1058.jschat.ui.add;

import com.andrei1058.jschat.Main;
import com.andrei1058.jschat.session.User;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AddView {

    /**
     * Show the add friend popup.
     */
    public AddView() {
        Stage window = new Stage();
        window.initOwner(Main.getWindow());
        window.initModality(Modality.APPLICATION_MODAL);

        VBox container = new VBox();

        // Add friend
        Text friend = new Text("Add a friend");
        VBox.setMargin(friend, new Insets(10, 0, 0, 10));
        container.getChildren().add(friend);
        TextField friendInput = new TextField();
        friendInput.setPromptText("Insert username");
        VBox.setMargin(friendInput, new Insets(0, 10, 0, 10));
        container.getChildren().add(friendInput);

        Button addFriend = new Button("Add friend");
        VBox.setMargin(addFriend, new Insets(0, 0, 0, 10));
        container.getChildren().add(addFriend);

        // Create group
        Text group = new Text("Create a group");
        VBox.setMargin(group, new Insets(10, 0, 0, 10));
        container.getChildren().add(group);
        TextField groupInput = new TextField();
        groupInput.setPromptText("Insert a name");
        VBox.setMargin(groupInput, new Insets(10, 10, 0, 10));
        container.getChildren().add(groupInput);

        Button createGroup = new Button("Create group");
        VBox.setMargin(createGroup, new Insets(0, 0, 0, 10));
        container.getChildren().add(createGroup);

        // Response
        HBox bottom = new HBox();
        VBox.setMargin(bottom, new Insets(10, 0, 0, 0));
        bottom.setAlignment(Pos.CENTER);
        Text response = new Text();
        bottom.getChildren().add(response);
        container.getChildren().add(bottom);


        // Listeners
        friendInput.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                if (!response.getText().isEmpty()) response.setText("");
                if (User.getUser().addFriend(friendInput.getText())) window.close();
                else {
                    response.setText("Invalid username!");
                    response.setFill(Color.RED);
                }
            }
        });
        addFriend.setOnMouseClicked(e -> {
            if (!response.getText().isEmpty()) response.setText("");
            if (User.getUser().addFriend(friendInput.getText())) window.close();
            else {
                response.setText("Invalid username!");
                response.setFill(Color.RED);
            }
        });

        groupInput.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                if (!response.getText().isEmpty()) response.setText("");
                if (User.getUser().createGroup(friendInput.getText())) window.close();
                else {
                    response.setText("Invalid group name!");
                    response.setFill(Color.RED);
                }
            }
        });
        createGroup.setOnMouseClicked(e -> {
            if (!response.getText().isEmpty()) response.setText("");
            if (User.getUser().createGroup(friendInput.getText())) window.close();
            else {
                response.setText("Invalid group name!");
                response.setFill(Color.RED);
            }
        });


        Scene mainScene = new Scene(container);
        window.setScene(mainScene);
        window.setMinWidth(300);
        window.setMinHeight(200);
        window.getIcons().add(Main.getWindow().getIcons().get(0));
        window.setResizable(false);
        window.show();
    }
}
