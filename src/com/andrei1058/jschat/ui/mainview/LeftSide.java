package com.andrei1058.jschat.ui.mainview;

import com.andrei1058.jschat.Main;
import com.andrei1058.jschat.session.Friend;
import com.andrei1058.jschat.ui.Refreshable;
import com.andrei1058.jschat.utils.Logger;
import com.sun.prism.paint.Paint;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;

public class LeftSide implements Refreshable {

    private StackPane parent;
    private MainView mainView;
    private VBox container = new VBox();
    private TabPane chatTabPane;
    private HBox inputContainer = new HBox();

    private TextField textField = new TextField();
    private Button sendButton;

    // Track selected tab
    // Username
    private String selectedTab = null;

    private HashMap<String, Tab> tabByIdentifier = new HashMap<>();

    public LeftSide(MainView mainView, StackPane parent) {
        this.parent = parent;
        this.mainView = mainView;
        parent.getChildren().add(container);

        //Tab noChatTab = new Tab("andrei1058");
        //new ChatTab(noChatTab);
        chatTabPane = new TabPane();

        container.getChildren().add(chatTabPane);
        VBox.setVgrow(chatTabPane, Priority.ALWAYS);
        container.getChildren().add(inputContainer);

        parent.setStyle("-fx-border-color: #D0D0D0;");

        inputContainer.getChildren().add(textField);
        //textField.setPadding(new Insets(0, 0, 3.05, 0));

        HBox.setHgrow(textField, Priority.ALWAYS);
        textField.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                Friend f = Friend.getByName(selectedTab);
                if (f == null) return;
                f.sendMessage(textField);
            }
        });

        addSendButton();

        setSelectedTab(null);
    }

    private void addSendButton() {
        sendButton = new Button("Send");
        sendButton.setAlignment(Pos.BOTTOM_RIGHT);
        sendButton.setPadding(new Insets(4, 30, 3.05, 30));
        HBox.setHgrow(sendButton, Priority.ALWAYS);
        inputContainer.getChildren().add(sendButton);

        sendButton.setOnMouseClicked(e -> {
            Friend f = Friend.getByName(selectedTab);
            if (f == null) return;
            f.sendMessage(textField);
        });
    }

    private void setSelectedTab(String username) {
        selectedTab = username;
        if (username == null || username.isEmpty()) {
            container.setDisable(true);
        } else {
            container.setDisable(false);
        }
    }

    /**
     * Show a chat with a friend or open it.
     */
    public void showChat(String username) {
        if (selectedTab != null && selectedTab.equals(username)) return;
        if (tabByIdentifier.containsKey("friend_" + username)) {
            setSelectedTab(username);
            chatTabPane.getSelectionModel().select(tabByIdentifier.get("friend_" + username));
            return;
        }
        Tab tab = new Tab(username);

        ScrollPane tabContainer = new ScrollPane();

        VBox vBox = new VBox();
        vBox.setPadding(new Insets(5, 0, 5, 5));
        vBox.setSpacing(4);
        setSelectedTab(username);

        Main.refreshUI(() -> {
            tabContainer.setContent(vBox);
            tab.setContent(tabContainer);
            tab.setOnSelectionChanged(e -> {
                setSelectedTab(username);
                textField.requestFocus();
            });
            tab.setOnClosed(e -> {
                if (chatTabPane.getTabs().isEmpty()) {
                    MainView.getMainView().getLeftSide().setSelectedTab(null);
                } else {
                    textField.requestFocus();
                }
                tabByIdentifier.remove("friend_" + username);
            });
            tab.setClosable(true);


            tabByIdentifier.put("friend_" + username, tab);
            chatTabPane.getTabs().add(0, tab);
            chatTabPane.getSelectionModel().select(0);
            textField.requestFocus();
        });
    }

    public void closeTab(String username) {
        Tab t = tabByIdentifier.get("friend_" + username);
        if (t == null) return;
        chatTabPane.getTabs().remove(t);
        if (chatTabPane.getTabs().isEmpty()) {
            MainView.getMainView().getLeftSide().setSelectedTab(null);
        } else {
            textField.requestFocus();
        }
        tabByIdentifier.remove("friend_" + username);
    }

    public void showMessage(Tab tab, String user, String message, boolean system) {
        Main.refreshUI(() -> {
            if (tab == null) return;
            VBox v = (VBox) ((ScrollPane) tab.getContent()).getContent();
            HBox msg = new HBox();
            VBox msg2 = new VBox();
            Text date = new Text(Main.dateFormat.format(new Date(System.currentTimeMillis())));
            date.setFont(Font.font("Verdana", FontPosture.ITALIC, 8));
            date.setOpacity(0.5);
            msg2.getChildren().add(date);

            HBox authorAndMessage = new HBox();
            msg2.getChildren().add(authorAndMessage);

            Text author = new Text(user + ": ");
            author.setFont(Font.font("Georgia", FontWeight.BOLD, 13));
            if (system) author.setFill(Color.RED);
            authorAndMessage.getChildren().add(author);

            Text meessg = new Text(message);
            meessg.setFont(Font.font("Sans-serif", 12));
            authorAndMessage.getChildren().add(meessg);

            msg.getChildren().add(msg2);

            v.getChildren().add(msg);
            VBox.setVgrow(msg, Priority.ALWAYS);
            ScrollPane sp = ((ScrollPane) tab.getContent());
            sp.layout();
            sp.setVvalue(1.0);
        });
    }


    @Override
    public void update() {
    }

    public HashMap<String, Tab> getTabByIdentifier() {
        return tabByIdentifier;
    }
}
