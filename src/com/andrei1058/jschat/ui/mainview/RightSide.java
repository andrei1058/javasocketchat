package com.andrei1058.jschat.ui.mainview;

import com.andrei1058.jschat.Main;
import com.andrei1058.jschat.session.Friend;
import com.andrei1058.jschat.session.User;
import com.andrei1058.jschat.ui.Refreshable;
import com.andrei1058.jschat.ui.add.AddView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.Date;
import java.util.HashMap;

public class RightSide implements Refreshable {

    private StackPane parent;
    private ScrollPane friendsScrollPane = new ScrollPane();
    private ScrollPane groupsScrollPane = new ScrollPane();
    private TabPane tabPane;
    private Tab friendsTab = new Tab();
    //private Tab groupsTab = new Tab();
    private MainView mainView;
    private double tabNameSize;
    private double tabPaneHeight;
    private double settingsPaneHeight;

    private StackPane settingsPane = new StackPane();

    private VBox groupsList;
    private VBox friendsList;

    private HashMap<String, Node> nodeByUsername = new HashMap<>();

    public RightSide(MainView mainView, StackPane parent) {
        Main.getWindow().setTitle("Java Socket Chat v" + Main.version);
        this.parent = parent;
        this.mainView = mainView;

        tabPane = new TabPane(friendsTab/*, groupsTab*/);

        friendsTab.setText("Friends");
        //groupsTab.setText("Groups");
        friendsTab.setClosable(false);
        //groupsTab.setClosable(false);
        updateTabPaneSize();
        tabPane.setTabMinWidth(tabNameSize);
        tabPane.setMinHeight(tabPaneHeight);
        tabPane.setMaxHeight(tabPaneHeight);
        tabPane.setStyle("-fx-border-color: #D0D0D0;");

        friendsList = new VBox();

        groupsList = new VBox();

        friendsScrollPane.setContent(friendsList);
        groupsScrollPane.setContent(groupsList);
        friendsTab.setContent(friendsScrollPane);
        //groupsTab.setContent(groupsScrollPane);

        parent.getChildren().add(tabPane);
        StackPane.setAlignment(tabPane, Pos.TOP_RIGHT);

        addOptionsPane();
    }

    /**
     * Calculate required dimensions for responsive design.
     */
    private void updateTabPaneSize() {
        tabNameSize = 43 * (mainView.getRightWidth() - 20) / 100;
        tabPaneHeight = 86 * mainView.getLeftHeight() / 100;
        settingsPaneHeight = 12 * mainView.getLeftHeight() / 100;
    }

    /**
     * Add options pane to the scene in the bottom right corner.
     */
    private void addOptionsPane() {

        parent.getChildren().add(settingsPane);
        settingsPane.setMinHeight(settingsPaneHeight);
        settingsPane.setMaxHeight(settingsPaneHeight);
        //settingsPane.setStyle("-fx-border-color: #D0D0D0;");
        StackPane.setAlignment(settingsPane, Pos.BOTTOM_RIGHT);

        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(30);
        settingsPane.getChildren().add(hBox);

        final ImageView s1 = new ImageView(new Image(RightSide.class.getResource("/settings1.png").toExternalForm()));
        s1.setCursor(Cursor.HAND);
        s1.setOnMouseEntered(e -> s1.setImage(new Image(RightSide.class.getResource("/settings2.png").toExternalForm())));
        s1.setOnMouseExited(e -> s1.setImage(new Image(RightSide.class.getResource("/settings1.png").toExternalForm())));
        hBox.getChildren().add(s1);

        final ImageView s2 = new ImageView(new Image(RightSide.class.getResource("/plus1.png").toExternalForm()));
        s2.setCursor(Cursor.HAND);
        s2.setOnMouseEntered(e -> s2.setImage(new Image(RightSide.class.getResource("/plus2.png").toExternalForm())));
        s2.setOnMouseExited(e -> s2.setImage(new Image(RightSide.class.getResource("/plus1.png").toExternalForm())));
        hBox.getChildren().add(s2);
        s2.setOnMouseClicked(e -> new AddView());

        final ImageView s3 = new ImageView(new Image(RightSide.class.getResource("/logout1.png").toExternalForm()));
        s3.setCursor(Cursor.HAND);
        s3.setOnMouseClicked(e -> User.getUser().logOut());
        s3.setOnMouseEntered(e -> s3.setImage(new Image(RightSide.class.getResource("/logout2.png").toExternalForm())));
        s3.setOnMouseExited(e -> s3.setImage(new Image(RightSide.class.getResource("/logout1.png").toExternalForm())));

        hBox.getChildren().add(s3);
        HBox.setHgrow(s3, Priority.ALWAYS);
    }

    @Override
    public void update() {
        updateTabPaneSize();
        tabPane.setTabMinWidth(tabNameSize);
        tabPane.setMinHeight(tabPaneHeight);
        tabPane.setMaxHeight(tabPaneHeight);
        settingsPane.setMinHeight(settingsPaneHeight);
        settingsPane.setMaxHeight(settingsPaneHeight);
    }

    public void friendRequest(String username) {

        HBox requestContainer = new HBox();
        ImageView imageView = new ImageView(new Image(RightSide.class.getResource("/new-friend-request.png").toExternalForm()));
        HBox.setMargin(imageView, new Insets(10, 0, 0, 0));
        requestContainer.getChildren().add(imageView);

        VBox vbox = new VBox();
        requestContainer.getChildren().add(vbox);
        requestContainer.setCursor(Cursor.HAND);
        HBox.setMargin(vbox, new Insets(5, 0, 0, 5));

        Text text = new Text(username);
        InnerShadow is = new InnerShadow();
        is.setOffsetX(4.0f);
        is.setOffsetY(4.0f);
        vbox.setMinHeight(30);
        text.setFont(new Font("Verdana", 15));
        DropShadow dropShadow = new DropShadow();
        dropShadow.setRadius(5.0);
        dropShadow.setOffsetX(3.0);
        dropShadow.setOffsetY(3.0);
        dropShadow.setColor(Color.color(0.4, 0.5, 0.5));
        text.setEffect(dropShadow);
        text.setFill(Color.web("0x3b596d"));

        Text add = new Text("New friend request.");

        vbox.getChildren().add(text);
        vbox.getChildren().add(add);
        //VBox.setMargin(text, new Insets(5, 0, 0, 2));
        /*if (!friendsList.getChildren().isEmpty()) {
            Text separator = new Text("╼╾╼╾╼╾╼╾╼╾╼");
            separator.setOpacity(0.3);
            friendsList.getChildren().add(0, separator);
        }*/
        friendsList.getChildren().add(0, requestContainer);

        // accept or refuse window
        requestContainer.setOnMouseClicked(e -> {
            Stage w2 = new Stage();
            w2.initOwner(Main.getWindow());
            w2.initModality(Modality.APPLICATION_MODAL);

            VBox mainContainer = new VBox();
            mainContainer.setAlignment(Pos.CENTER);

            Text request = new Text("New friend request from " + username);
            mainContainer.getChildren().add(request);

            HBox buttonsContainer = new HBox();
            VBox.setMargin(buttonsContainer, new Insets(10, 0, 0, 0));
            mainContainer.getChildren().add(buttonsContainer);

            ImageView accept = new ImageView(new Image(RightSide.class.getResource("/request-accept.png").toExternalForm()));
            accept.setCursor(Cursor.HAND);
            accept.setOnMouseClicked(ee -> {
                User.getUser().manageFriendRequest(username, true);
                friendsList.getChildren().remove(requestContainer);
                w2.close();
            });
            ImageView deny = new ImageView(new Image(RightSide.class.getResource("/request-deny.png").toExternalForm()));
            deny.setCursor(Cursor.HAND);
            deny.setOnMouseClicked(ee -> {
                User.getUser().manageFriendRequest(username, false);
                friendsList.getChildren().remove(requestContainer);
                w2.close();
            });

            buttonsContainer.getChildren().addAll(accept, deny);
            buttonsContainer.setSpacing(10);
            buttonsContainer.setAlignment(Pos.CENTER);

            Scene mainScene = new Scene(mainContainer);
            w2.setScene(mainScene);
            w2.setMinWidth(300);
            w2.setMinHeight(200);
            w2.getIcons().add(Main.getWindow().getIcons().get(0));
            w2.setResizable(false);
            w2.show();
        });
    }

    public void addFriendToList(String username, int onlineStatus, long lastRead) {

        Main.refreshUI(() -> {
            HBox mainContainer = new HBox();
            ImageView imageView = new ImageView(new Image(RightSide.class.getResource((onlineStatus == 1 ? "/online-user.png" : "/offline-user.png")).toExternalForm()));
            HBox.setMargin(imageView, new Insets(10, 0, 0, 0));
            mainContainer.getChildren().add(imageView);

            VBox vbox = new VBox();
            mainContainer.getChildren().add(vbox);
            mainContainer.setCursor(Cursor.HAND);
            HBox.setMargin(vbox, new Insets(5, 0, 0, 5));

            Text text = new Text(username);
            InnerShadow is = new InnerShadow();
            is.setOffsetX(4.0f);
            is.setOffsetY(4.0f);
            vbox.setMinHeight(30);
            text.setFont(new Font("Verdana", 15));
            DropShadow dropShadow = new DropShadow();
            dropShadow.setRadius(5.0);
            dropShadow.setOffsetX(3.0);
            dropShadow.setOffsetY(3.0);
            dropShadow.setColor(Color.color(0.4, 0.5, 0.5));
            text.setEffect(dropShadow);
            text.setFill(Color.web("0x3b596d"));
            vbox.getChildren().add(text);

            Text date = new Text(Main.dateFormat.format(new Date(System.currentTimeMillis())));
            date.setFont(Font.font("Verdana", FontPosture.ITALIC, 8));
            date.setOpacity(0.5);
            vbox.getChildren().add(date);

            friendsList.getChildren().add(mainContainer);
            //on click
            mainContainer.setOnMouseClicked(e -> {
                if (e.getButton() == MouseButton.PRIMARY) {
                    User.getUser().openConversation(username);
                } else if (e.getButton() == MouseButton.SECONDARY) {
                    removeFriend(username);
                }
            });

            nodeByUsername.put("friend_" + username, mainContainer);
        });
    }

    private void removeFriend(String username) {
        Node friendNode = nodeByUsername.getOrDefault("friend_" + username, null);
        if (friendNode == null) return;
        Stage w2 = new Stage();
        w2.initOwner(Main.getWindow());
        w2.initModality(Modality.APPLICATION_MODAL);

        VBox mainContainer = new VBox();
        mainContainer.setAlignment(Pos.CENTER);

        Text request = new Text("Remove user from friends list: " + username);
        mainContainer.getChildren().add(request);

        HBox buttonsContainer = new HBox();
        VBox.setMargin(buttonsContainer, new Insets(10, 0, 0, 0));
        mainContainer.getChildren().add(buttonsContainer);

        ImageView accept = new ImageView(new Image(RightSide.class.getResource("/request-deny.png").toExternalForm()));
        accept.setCursor(Cursor.HAND);
        accept.setOnMouseClicked(ee -> {
            User.getUser().removeFriend(username);
            Friend f = Friend.getByName(username);
            if (f != null) f.remove();
            friendsList.getChildren().remove(friendNode);
            MainView.getMainView().getLeftSide().closeTab(username);
            w2.close();
        });
        ImageView deny = new ImageView(new Image(RightSide.class.getResource("/request-accept.png").toExternalForm()));
        deny.setCursor(Cursor.HAND);
        deny.setOnMouseClicked(ee -> {
            w2.close();
        });

        buttonsContainer.getChildren().addAll(accept, deny);
        buttonsContainer.setSpacing(10);
        buttonsContainer.setAlignment(Pos.CENTER);

        Scene mainScene = new Scene(mainContainer);
        w2.setScene(mainScene);
        w2.setMinWidth(300);
        w2.setMinHeight(200);
        w2.getIcons().add(Main.getWindow().getIcons().get(0));
        w2.setResizable(false);
        w2.show();
    }

    public VBox getFriendsList() {
        return friendsList;
    }

    public HashMap<String, Node> getNodeByUsername() {
        return nodeByUsername;
    }
}
