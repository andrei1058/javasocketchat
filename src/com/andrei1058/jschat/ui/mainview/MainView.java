package com.andrei1058.jschat.ui.mainview;

import com.andrei1058.jschat.Main;
import com.andrei1058.jschat.ui.Refreshable;
import com.andrei1058.jschat.ui.login.LoginView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class MainView implements Refreshable {

    private static MainView mainView;

    private StackPane left, right;
    private HBox mainLayout;

    private int rightWidth, leftHeight;

    private RightSide rightSide;
    private LeftSide leftSide;

    public MainView() {
        Stage window = Main.getWindow();
        mainView = this;

        // Remove any trace of login screen
        if (LoginView.getInstance() != null) LoginView.setInstance(null);
        // Remove any trace of login screen
        if (LoginView.getInstance() != null) LoginView.setInstance(null);

        mainLayout = new HBox();
        mainLayout.setPadding(new Insets(20, 20, 20, 20));
        mainLayout.setSpacing(10);

        updateDimensions();

        left = new StackPane();
        right = new StackPane();

        // Chat pane
        left.setAlignment(Pos.BASELINE_LEFT);
        leftSide = new LeftSide(this, left);
        mainLayout.getChildren().add(left);
        HBox.setHgrow(left, Priority.ALWAYS);

        // Friends and Groups pane
        //right.setBackground(new Background(new BackgroundImage(
        //        new Image(MainView.class.getResource("/background.jpg").toExternalForm()), BackgroundRepeat.REPEAT,
        //        BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(right.getWidth(), right.getHeight(),
        //        false, false, false, false))));
        mainLayout.getChildren().add(right);
        right.setAlignment(Pos.BASELINE_RIGHT);
        HBox.setHgrow(right, Priority.ALWAYS);
        rightSide = new RightSide(this, right);

        Scene scene = new Scene(mainLayout);
        scene.widthProperty().addListener(e -> update());
        scene.heightProperty().addListener(e -> update());
        window.setScene(scene);
    }

    private void updateDimensions() {
        rightWidth = (int) (30 * (Main.getWindow().getWidth() - ((mainLayout.getPadding().getLeft() * 3) + mainLayout.getSpacing())) / 100);
        leftHeight = (int) (Main.getWindow().getHeight() - (mainLayout.getPadding().getTop() * 3) - (mainLayout.getSpacing() * 2));
    }

    public double getRightWidth() {
        return rightWidth;
    }

    public double getLeftHeight() {
        return leftHeight;
    }

    @Override
    public void update() {
        updateDimensions();
        left.setMinHeight(getLeftHeight());
        left.setMaxHeight(getLeftHeight());
        right.setMaxWidth(getRightWidth());
        right.setMinWidth(getRightWidth());
        left.setMaxWidth(70 * (Main.getWindow().getWidth() - ((mainLayout.getPadding().getLeft() * 3) + mainLayout.getSpacing())) / 100);
        left.setMinWidth(70 * (Main.getWindow().getWidth() - ((mainLayout.getPadding().getLeft() * 3) + mainLayout.getSpacing())) / 100);
        right.setMaxHeight(Main.getWindow().getHeight() - (mainLayout.getPadding().getTop() * 3) - (mainLayout.getSpacing() * 2));
        right.setMinHeight(Main.getWindow().getHeight() - (mainLayout.getPadding().getTop() * 3) - (mainLayout.getSpacing() * 2));
        rightSide.update();
        leftSide.update();
    }

    public void newFriendRequest(String username){
        Main.refreshUI(()-> rightSide.friendRequest(username));
    }

    public void addFriendToList(String username, int onlineStatus, long lastRead){
        rightSide.addFriendToList(username, onlineStatus, lastRead);
    }

    public static MainView getMainView() {
        return mainView;
    }

    public LeftSide getLeftSide() {
        return leftSide;
    }

    public RightSide getRightSide() {
        return rightSide;
    }
}
