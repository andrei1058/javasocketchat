package com.andrei1058.jschat.ui.register;

import com.andrei1058.jschat.Main;
import com.andrei1058.jschat.errors.*;
import com.andrei1058.jschat.session.SessionValidator;
import com.andrei1058.jschat.ui.login.LoginView;
import com.sun.org.apache.regexp.internal.RE;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.net.UnknownHostException;

public class RegisterView {

    private Text bottomMessage = new Text("");
    private static RegisterView instance;

    public RegisterView() {

        // Remove any trace of login screen
        if (LoginView.getInstance() != null) LoginView.setInstance(null);

        instance = this;

        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(new ImageView(new Image(RegisterView.class.getResource("/header.png").toExternalForm())));

        // Bottom main layout
        VBox firstBottom = new VBox();
        HBox bottomContainer = new HBox();
        firstBottom.getChildren().add(bottomContainer);
        firstBottom.getChildren().add(bottomMessage);
        borderPane.setBottom(firstBottom);
        firstBottom.setAlignment(Pos.CENTER);
        bottomContainer.setMaxWidth(600);
        bottomContainer.setPadding(new Insets(0, 0, 50, 0));
        bottomContainer.setSpacing(18);

        // Layout containing username fields
        VBox usernameContainer = new VBox();
        bottomContainer.getChildren().add(usernameContainer);

        Text username = new Text("Username");
        usernameContainer.getChildren().add(username);
        TextField insertUsername = new TextField();
        insertUsername.setPromptText("Insert username");
        usernameContainer.getChildren().add(insertUsername);
        VBox.setMargin(insertUsername, new Insets(0, 0, 10, 0));

        Text email = new Text("Recovery Email");
        usernameContainer.getChildren().add(email);
        TextField recoveryEmail = new TextField();
        recoveryEmail.setPromptText("Insert recovery email");
        usernameContainer.getChildren().add(recoveryEmail);

        // Layout containing password fields
        VBox passContainer = new VBox();
        bottomContainer.getChildren().add(passContainer);

        Text passwd = new Text("Password");
        passContainer.getChildren().add(passwd);
        PasswordField password = new PasswordField();
        password.setPromptText("Insert password");
        passContainer.getChildren().add(password);
        VBox.setMargin(password, new Insets(0, 0, 10, 0));

        Text passwd2 = new Text("Repeat Password");
        passContainer.getChildren().add(passwd2);
        PasswordField repeatPassword = new PasswordField();
        repeatPassword.setPromptText("Repeat password");
        passContainer.getChildren().add(repeatPassword);

        // Third box
        HBox buttons = new HBox();
        bottomContainer.getChildren().add(buttons);
        //HBox.setHgrow(buttons, Priority.ALWAYS);
        buttons.setAlignment(Pos.BOTTOM_LEFT);
        buttons.setSpacing(30);

        Button register = new Button("Register");
        buttons.getChildren().add(register);
        Hyperlink login = new Hyperlink("Back to Login");
        login.setOnMouseClicked(e -> Main.refreshUI(LoginView::new));
        buttons.getChildren().add(login);
        login.setAlignment(Pos.BOTTOM_RIGHT);

        insertUsername.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                onListener(insertUsername.getText(), password.getText(), repeatPassword.getText(), recoveryEmail.getText());
            }
        });
        password.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                onListener(insertUsername.getText(), password.getText(), repeatPassword.getText(), recoveryEmail.getText());
            }
        });
        repeatPassword.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                onListener(insertUsername.getText(), password.getText(), repeatPassword.getText(), recoveryEmail.getText());
            }
        });
        recoveryEmail.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                onListener(insertUsername.getText(), password.getText(), repeatPassword.getText(), recoveryEmail.getText());
            }
        });
        register.setOnMouseClicked(e -> {
            onListener(insertUsername.getText(), password.getText(), repeatPassword.getText(), recoveryEmail.getText());
        });

        Scene scene = new Scene(borderPane, Main.getWindow().getScene().getWidth(), Main.getWindow().getScene().getHeight());
        Main.getWindow().setScene(scene);
    }

    private void onListener(String username, String password1, String password2, String email) {
        if (username.isEmpty()){
            showMessage("Please insert an username!", Color.RED);
            return;
        }
        if (password1.isEmpty()){
            showMessage("Please insert a password!", Color.RED);
            return;
        }
        if (password2.isEmpty()){
            showMessage("Please repeat your password!", Color.RED);
            return;
        }
        if (email.isEmpty()){
            showMessage("Please insert a recovery email!", Color.RED);
            return;
        }
        try {
            new SessionValidator(username, password1, password2, email);
        } catch (PasswordsMatchException e) {
            showMessage("Passwords don't match!", Color.RED);
        } catch (InvalidUsernameFormat invalidUsernameFormat) {
            showMessage("Invalid username format!", Color.RED);
        } catch (InvalidPassword invalidPassword) {
            showMessage("Invalid password format!", Color.RED);
        } catch (InvalidEmailException e) {
            showMessage("Invalid email address!", Color.RED);
        } catch (NoDatabaseConnection noDatabaseConnection) {
            showMessage("Could not create local database!", Color.RED);
        } catch (UnknownHostException e) {
            showMessage("Could not connect to the remote host!", Color.RED);
        }
    }

    /**
     * Show a message on bottom.
     */
    public static void showMessage(String message, Color color) {
        if (instance == null) return;
        if (instance.bottomMessage.getText().isEmpty()) {
            if (color == Color.RED) {
                new Thread(() -> {
                    try {
                        Thread.sleep(4000);
                        if (instance != null) {
                            instance.bottomMessage.setText("");
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }).start();
            }
        }
        instance.bottomMessage.setText(message);
        instance.bottomMessage.setFill(color);
    }

    public static RegisterView getInstance() {
        return instance;
    }

    public static void setInstance(RegisterView instance) {
        RegisterView.instance = instance;
    }
}
