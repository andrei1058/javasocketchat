package com.andrei1058.jschat.ui;

public interface Refreshable {

    void update();
}
