package com.andrei1058.jschat;

import com.andrei1058.jschat.session.SessionHandler;
import com.andrei1058.jschat.ui.login.LoginView;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Main extends Application {

    public static final double version = 0.1;
    public static boolean alive = true;
    public static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm");

    private static Stage window;

    @Override
    public void start(Stage primaryStage) {
        window = primaryStage;
        window.getIcons().add(new Image(Main.class.getResource("/icon.png").toExternalForm()));
        window.setOnCloseRequest(e -> {
            window.close();
            close();
        });
        new LoginView();
    }

    /**
     * Method used to refresh UI stuff.
     */
    public static void refreshUI(Runnable treatment) {
        if (Platform.isFxApplicationThread()) treatment.run();
        else Platform.runLater(treatment);
    }

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Get stage.
     */
    public static Stage getWindow() {
        return window;
    }

    /**
     * Close program.
     */
    public static void close() {
        alive = false;
        SessionHandler.closeConnection();
    }
}
